/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserProfile(){
	let fullName = prompt("what is your name?"); 
	let age = prompt("how old are you?"); 
	let location = prompt("where do you live?");
	alert("thanks for that information");
	console.log("hello " + fullName)
	console.log("you are " + age)
	console.log("you live in " +  location)

	};

printUserProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function top5favoriteband(){
	console.log("1. Eraserheads")
	console.log("2. BenNBEn")
	console.log("3. ParokyaniEdgar")
	console.log("4. Kameekazee")
	console.log("5. Mayonaise")
}

top5favoriteband();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function top5favoritemovies(){
	let rtrating = "Rotten tommatoes rating"
	console.log("1. The god father")
	console.log(rtrating + " 97")
	console.log("2. top Gun")
	console.log(rtrating + " 96")
	console.log("3. THE GOOD BOSS")
	console.log(rtrating + " 92")
	console.log("4. THE RIGHTEOUS")
	console.log(rtrating + " 92")
	console.log("5. DEADSTREAM ")
	console.log(rtrating + " 91")
}

top5favoritemovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();